# Intro to SQL
This is a repo to introduce sql and dbs

Objectives
Intro to sql and db
Install mysql and workbench 
Define a table and columns

introduction to sql and db
what is a database?
a file with inforamtion
anything can be a database if it is keeping data but some of thema re harder to find the right data than others. In technology this would not be what people are refering to when they talk about databases.

Usually it is SQL or NoSQL

## SQL Structured Query Language
how is it structured?
it has tables with connections between them. This makes it easy to query and get out the information we want

### Common terminology
DB database
sql  how a db might be organised
dbs database management system
Columns
rows
tables


Books
| book_id | title                  | author_id | date | price | genre     |
|-----------|------------------------|-----------|------|-------|-----------|
| 1         | Harry Potter           | 1         | 2001 | 12    | fiction   |
| 2         | Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| 3         | Good Vibes - Good life | 2         | 2020 | 20    | Self Help |


Authors
| author_id | First_name | Last_name | email              |   |   |
|-----------|------------|-----------|--------------------|---|---|
| 1         | J. K.      | Rowling   | jk@gmail.com       |   |   |
| 2         | Harry      | Bookster  | hb@bookster.com    |   |   |
| 3         | Vex        | King      | king@kingbooks.com |   |   |

Genres
| genre_id  | type      | description                                 | other_info |   |   |
|-----------|-----------|---------------------------------------------|------------|---|---|
| 1         | fiction   | these are fictional noval                   | none       |   |   |
| 2         | Biography | some what true accounts of people's lives   | none       |   |   |
| 3         | Selfhelp  | Meant to help you think and better yourself | none       |   |   |

We can see how they connect because we can see context but a machine needs to use primary keys and foreign keys
every table have a column of primary keys which can not be empty
primary keys are unique and cannot be reused
forgein keys are a reference to a primary key from another table which links the two together in a way

sql data types
Different data types: Charindex, charmax, decimals, integers

the keys create relations: one to one, one to many, many to many